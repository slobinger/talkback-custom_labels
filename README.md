# Custom Labels for TalkBack
TalkBack is an accassability app for Android, which enables blind people
to hear what is on the screen.

While it is possible to write completely accessible Apps, by labeling
UI-elements, many apps out there are not developed like this.

Therefor TalkBack provides the 'custom labels' feature. It can be reached by the
'local context menu', where one can add, modify or delete a custom label.
TalkBack also provides a feature to import and export this custom labels.

This project provides custom labels for several apps in multiple languages to
help blind people to understand what all these 'untitled buttons' are.